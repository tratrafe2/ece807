%{
% This program is responsible for reading .csv files as outputed from the
% java RSA implementation for the ECE807 project. It then plots the 
% appropriate figures and finally saves them to a folder. In order to run,
% the files and folders need to be set up correctly.
%}
%%
clear all;

%% Read files
T(:,:,1) = readmatrix('../metrics/metrics_111.csv');
T(:,:,2) = readmatrix('../metrics/metrics_112.csv');
T(:,:,3) = readmatrix('../metrics/metrics_113.csv');
T(:,:,4) = readmatrix('../metrics/metrics_121.csv');
T(:,:,5) = readmatrix('../metrics/metrics_122.csv');
T(:,:,6) = readmatrix('../metrics/metrics_123.csv');
T(:,:,7) = readmatrix('../metrics/metrics_131.csv');
T(:,:,8) = readmatrix('../metrics/metrics_132.csv');
T(:,:,9) = readmatrix('../metrics/metrics_133.csv');
T(:,:,10) = readmatrix('../metrics/metrics_211.csv');
T(:,:,11) = readmatrix('../metrics/metrics_212.csv');
T(:,:,12) = readmatrix('../metrics/metrics_213.csv');
T(:,:,13) = readmatrix('../metrics/metrics_221.csv');
T(:,:,14) = readmatrix('../metrics/metrics_222.csv');
T(:,:,15) = readmatrix('../metrics/metrics_223.csv');
T(:,:,16) = readmatrix('../metrics/metrics_231.csv');
T(:,:,17) = readmatrix('../metrics/metrics_232.csv');
T(:,:,18) = readmatrix('../metrics/metrics_233.csv');
T(:,:,19) = readmatrix('../metrics/metrics_311.csv');
T(:,:,20) = readmatrix('../metrics/metrics_312.csv');
T(:,:,21) = readmatrix('../metrics/metrics_313.csv');

%% Strings
st1 = "First Fit heuristics for Dijkstra with Link disjoint backups";
st2 = "Backup methods for Dijkstra with FIFO for First Fit";
st3 = "Algorithms for Link disjoint backups with FIFO for First Fit";

titles=[
    "Dijkstra, Link Disjoint, Path Cost heuristic";
    "Dijkstra, Link Disjoint, Slots Number heuristic";
    "Dijkstra, Link Disjoint, FIFO heuristic";
    "Dijkstra, Node&Link, Disjoint Path Cost heuristic";
    "Dijkstra, Node&Link, Disjoint Slots Number heuristic";
    "Dijkstra, Node&Link, Disjoint FIFO heuristic";
    "Dijkstra, No Backup, Path Cost heuristic";
    "Dijkstra, No Backup, Slots Number heuristic";
    "Dijkstra, No Backup, FIFO heuristic";
    
    "Bellman-Ford, Link Disjoint, Path Cost heuristic";
    "Bellman-Ford, Link Disjoint, Slots Number heuristic";
    "Bellman-Ford, Link Disjoint, FIFO heuristic";
    "Bellman-Ford, Node&Link, Disjoint Path Cost heuristic";
    "Bellman-Ford, Node&Link, Disjoint Slots Number heuristic";
    "Bellman-Ford, Node&Link, Disjoint FIFO heuristic";
    "Bellman-Ford, No Backup, Path Cost heuristic";
    "Bellman-Ford, No Backup, Slots Number heuristic";
    "Bellman-Ford, No Backup, FIFO heuristic";
    
    "Suurballe, Link Disjoint, Path Cost heuristic";
    "Suurballe, Link Disjoint, Slots Number heuristic";
    "Suurballe, Link Disjoint, FIFO heuristic";
];

names=[
  "Initial Connetions";
  "Routed connections";
  "Computation time (ms)";
  
  "Primary demands";
  "Routed Primary demands";
  "Utilized slots of Primary demands";
  
  "Backup demands";
  "Routed Backup demands";
  "Utilized slots of Backup demands";
  
  "Not routed connections";
  "Connection Blocking Rate (%)";
];



%% Calculate not routed and blocking rate metrics
for i=1:size(T,3)
    T(:,10,i)=T(:,1,i)-T(:,2,i);
    T(:,11,i)=100*T(:,10,i)./T(:,1,i);
end

%% Plot graphs
X=[3;10;11];
%% Heuristics
plotColumns(X,names,"Dijkstra","Link Disjoint","",T(:,:,1),T(:,:,2),T(:,:,3),'Path Cost heuristic','Slots Number heuristic','FIFO');
plotColumns(X,names,"Bellman-Ford","Link Disjoint","",T(:,:,10),T(:,:,11),T(:,:,12),'Path Cost heuristic','Slots Number heuristic','FIFO');
plotColumns(X,names,"Suurballe","Link Disjoint","",T(:,:,19),T(:,:,20),T(:,:,21),'Path Cost heuristic','Slots Number heuristic','FIFO');
%% Backup methods
plotColumns(X,names,"Dijkstra","","Path Cost heuristic",T(:,:,1),T(:,:,4),T(:,:,7),'Link disjoint','Link & Node disjoint','No backup');
plotColumns(X,names,"Dijkstra","","Slots Number heuristic",T(:,:,2),T(:,:,5),T(:,:,8),'Link disjoint','Link & Node disjoint','No backup');
plotColumns(X,names,"Dijkstra","","FIFO",T(:,:,3),T(:,:,6),T(:,:,9),'Link disjoint','Link & Node disjoint','No backup');
%% Algorithms
plotColumns(X,names,"","Link Disjoint","Path Cost heuristic",T(:,:,1),T(:,:,10),T(:,:,19),'Dijkstra','Bellman-Ford','Suurballe');
plotColumns(X,names,"","Link Disjoint","Slots Number heuristic",T(:,:,2),T(:,:,11),T(:,:,20),'Dijkstra','Bellman-Ford','Suurballe');
plotColumns(X,names,"","Link Disjoint","FIFO",T(:,:,3),T(:,:,12),T(:,:,21),'Dijkstra','Bellman-Ford','Suurballe');

%% Plot Utilized Slots
for counter=1:size(T,3)
    t=T(:,:,counter);
    name={'Utilized Slots',titles(counter)};
    figure('Name',strcat(name{1}," ",name{2}));
    title(name);
    xlabel("Number of connections");
    ylabel('Utilized Slots');
    x=t(:,1);
    y1=t(:,6)+t(:,9);
    y2=t(:,6);
    hold on;
    axis([0 3000 0 8000])
    bar(x,y1);
    bar(x,y2);
    hold off;
    lg=legend('Primary + Backup','Primary');
    lg.Position=[0.7 0.2 0.1 0.1];
end

%% Save plots 
folder = "../figures"; 
figList = findobj(allchild(0), 'flat', 'Type', 'figure');
for i = 1:length(figList)
  figHandle = figList(i);
  figName   = get(figHandle, 'Name');
  saveas(figHandle, fullfile(folder, strcat(figName,int2str(i),'.png')));
end

%% Functions
function [] = plotColumns(X,names,aglo,backup,heuristic,T1,T2,T3,s1,s2,s3)
    for counter=1:size(X,1)
        i=X(counter);
        name={names(i),strcat(aglo," ",backup," ",heuristic)};
        figure('Name',strcat(name{1}," - ",name{2}));
        title(name);
        xlabel("Number of connections");
        ylabel(names(i));
        hold on;
        plot(T1(:,1),T1(:,i));
        plot(T2(:,1),T2(:,i),'-.');
        plot(T3(:,1),T3(:,i));
        hold off;
        lg=legend(s1,s2,s3);
        lg.Position=[0.7 0.2 0.1 0.1];
    end
end