import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The Main class is responsible for all the interaction with the user and for
 * calling the solver when all necessary information have been gathered. It asks
 * for the path of the graph file and the demands file, as well as the code for
 * the type of the algorithm, the type of the backup plan and the first-fit
 * heuristic. It also needs to know the initial and final numbers of demands to
 * read from a file, so that it creates an iteration of experiments that contain
 * those demands. After that, it calls the solver in a loop in order to examine
 * different numbers of demands. It times each solution and holds the results
 * using a Log object, in order to later print them, along with some metrics
 * regarding the performance of each iteration.
 * 
 * @author cmakri07
 *
 */
public class Main {
	/**
	 * The main function that runs the program as described in the class'
	 * description.
	 * 
	 * @param args The initial arguments of the program.
	 */
	public static void main(String[] args) {
		args = readArgs(args);

		StringBuilder sb_log = new StringBuilder();
		StringBuilder sb_csv = new StringBuilder();

		// Read inputs
		String graphFile = args[0];
		String demandsFile = args[1];
		Demand.Algorithm algorithm = Demand.Algorithm.getAlgorithm(args[5]);
		Demand.BackupPlan backupPlan = Demand.BackupPlan.getBackupPlan(args[6]);
		Demand.FFHeuristic ffHeuristic = Demand.FFHeuristic.getFFHeuristic(args[7]);
		Log.logInputs(graphFile, demandsFile, algorithm, backupPlan, ffHeuristic, sb_log);

		// Read files
		Graph g = Graph.graphFromFile(graphFile);
		List<Demand> demandsList = Demand.demandsFromFile(demandsFile);

		// Find the numbers to read
		int initDemands = Integer.parseInt(args[2]);
		if (initDemands > demandsList.size())
			initDemands = demandsList.size();
		int endDemands = Integer.parseInt(args[3]);
		if (endDemands > demandsList.size())
			endDemands = demandsList.size();
		else if (endDemands < initDemands)
			endDemands = demandsList.size();
		if (initDemands < 1) {
			initDemands = demandsList.size();
			endDemands = demandsList.size();
		}
		// Iterate number of demands
		for (; initDemands <= endDemands; initDemands += 1) {
			// Solve
			List<Connection> connectionList = new ArrayList<Connection>();
			long time = System.currentTimeMillis();

			ArrayList<Link> usedLinks = Link.solveRSA(g, connectionList, demandsList, algorithm, backupPlan,
					ffHeuristic, initDemands);

			time = System.currentTimeMillis() - time;
			// Metrics
			Log log = new Log(connectionList, usedLinks, initDemands, time);
			if (args[4].equals("1") || initDemands == endDemands)
				log.logData(sb_log);
			log.csvData(sb_csv);
		}

		String postfix = args[5] + "" + args[6] + "" + args[7];
		printSB("log_" + postfix + ".txt", sb_log);
		printSB("metrics_" + postfix + ".csv", sb_csv);

		System.out.println("Terminated.");
	}

	/**
	 * A wizard to help the user input the missing arguments to the main program.
	 * 
	 * @param args_old The initial arguments of the program.
	 * @return The altered arguments of the program.
	 */
	private static String[] readArgs(String[] args_old) {
		String[] args = new String[8];
		for (int i = 0; i < args_old.length; i++)
			args[i] = args_old[i];
		Scanner sc = new Scanner(System.in);
		try {
			if (args_old.length < 1) {
				System.out.println("Please enter the graph's file path:");
				args[0] = sc.nextLine();
			}
			if (args_old.length < 2) {
				System.out.println("Please enter the demand's file path:");
				args[1] = sc.nextLine();
			}
			if (args_old.length < 3) {
				System.out.println("Please give M the number of initial demands for the first iteration.");
				System.out.println("( 1 <= M <= max_demands )");
				args[2] = sc.nextInt() + "";
			}
			if (args_old.length < 4) {
				System.out.println("Please give N the number of initial demands for the last iteration.");
				System.out.println("( M <= N <= max_demands )");
				args[3] = sc.nextInt() + "";
			}
			if (args_old.length < 5) {
				System.out.println("Do you want the result to be output into a log file?");
				System.out.println("(Select No for large number of iterations or set higher Heap memory)");
				System.out.println("1. Yes. Save all iterations.");
				System.out.println("2. No. Save only last iteration.");
				args[4] = sc.nextInt() + "";
			}
			if (args_old.length < 6) {
				System.out.println("Please choose the routing algorithm:");
				System.out.println("1. Dijkstra");
				System.out.println("2. Bellman-Ford");
				System.out.println("3. Suurballe");
				args[5] = sc.nextInt() + "";
			}
			if (args[5].equals("3")) {
				args[6] = "1";
			} else if (args_old.length < 7) {
				System.out.println("Please choose the backup method:");
				System.out.println("1. Link Disjoint");
				System.out.println("2. Node & Link Disjoint");
				System.out.println("3. No backup");
				args[6] = sc.nextInt() + "";
			}
			if (args_old.length < 8) {
				System.out.println("Please choose the First-Fit heuristic:");
				System.out.println("1. Descending Path size");
				System.out.println("2. Descending Number of slots");
				System.out.println("3. No heuristic (FIFO primary demands, then FIFO backup demands)");
				args[7] = sc.nextInt() + "";
			}
		} catch (Exception e) {
			System.out.println("Wrong input detected.");
			sc.close();
			System.exit(5);
		}
		sc.close();
		return args;
	}

	private static void printSB(String fileName, StringBuilder sb) {
		try {
			PrintStream ps_log = new PrintStream(new File(fileName));
			ps_log.print(sb);
			ps_log.flush();
			ps_log.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
