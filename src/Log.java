import java.util.ArrayList;
import java.util.List;

/**
 * This class exists for the creation of a log object that keeps track of the
 * metrics of an instance of a solution. This is essential in order to be able
 * to keep the results of each iteration of the loop that calls the solve
 * function with different numbers of demands each time.
 * 
 * @author cmakri07
 *
 */
public class Log {

	ArrayList<Link> usedLinks;
	List<Connection> connectionList;
	int initConnections;
	long time;

	int connections = 0;
	int routedConnections = 0;

	int primaryDemands = 0;
	int primaryRouted = 0;
	int primarySlots = 0;

	int backupDemands = 0;
	int backupRouted = 0;
	int backupSlots = 0;

	/**
	 * The constructor of the Log object.
	 * 
	 * @param connectionList  The list of connections.
	 * @param usedLinks       The list of Used links.
	 * @param initConnections The number of initial demands.
	 * @param time            The computation time.
	 */
	Log(List<Connection> connectionList, ArrayList<Link> usedLinks, int initConnections, long time) {
		this.connectionList = connectionList;
		this.usedLinks = usedLinks;
		this.initConnections = initConnections;
		this.time = time;
		calcMetrics();

	}

	/**
	 * A private helping function to calculate this log's metrics.
	 */
	private void calcMetrics() {
		for (Connection con : connectionList) {
			if (con.isRouted() == true)
				routedConnections++;
			for (Demand d : con.getDemands()) {
				if (d.path.size() > 0) {
					if (d.getPrimary() == true) {
						this.primaryDemands++;
						if (d.getRouted()) {
							this.primarySlots += d.slotsNumber;
							this.primaryRouted++;
						}
					} else {
						this.backupDemands++;
						if (d.getRouted()) {
							this.backupSlots += d.slotsNumber;
							this.backupRouted++;
						}
					}
				}
			}
		}
	}

	/**
	 * A static function to log the inputs of an instance.
	 * 
	 * @param graphFile   The graph file path.
	 * @param demandsFile The demands file path.
	 * @param algorithm   The algorithm
	 * @param backupPlan  The backup method
	 * @param ffHeuristic The heuristic of the first fit algorithm
	 * @param sb_log      The StringBuilder to append this information.
	 */
	public static void logInputs(String graphFile, String demandsFile, Demand.Algorithm algorithm,
			Demand.BackupPlan backupPlan, Demand.FFHeuristic ffHeuristic, StringBuilder sb_log) {
		sb_log.append("Graph file: " + graphFile + "\n");
		sb_log.append("Demands file: " + demandsFile + "\n");
		sb_log.append("Routing algorithm: " + algorithm + "\n");
		sb_log.append("Backup Method: " + backupPlan + "\n");
		sb_log.append("First-Fit heuristic: " + ffHeuristic + "\n");
	}

	/**
	 * A function to log the properties of a log object into a stringBuilder.
	 * 
	 * @param sb_log The StringBuilder to append the log's information.
	 */
	public void logData(StringBuilder sb_log) {
		sb_log.append("\nPathID\tSlots\tBitrate\tPath\n");
		List<Demand> demands = Connection.sortAllDemandsID(this.connectionList);
		for (Demand d : demands)
			sb_log.append(d + "\n");

		sb_log.append("\nLinks\n");
		for (Link l : usedLinks)
			if (l.slots != null)
				sb_log.append(l + "\n");

		double connectionBlockingRate = 100.0 * (initConnections - routedConnections) / initConnections;

		sb_log.append("\n");
		sb_log.append("Number of initial demands: " + this.initConnections + "\n");
		sb_log.append("Number of routed connections: " + this.routedConnections + "\n");
		sb_log.append("\nComputation time: " + this.time + " ms" + "\n");

		sb_log.append("\nNumber of primary demands: " + this.primaryDemands + "\n");
		sb_log.append("Number of routed primary demands: " + this.primaryRouted + "\n");
		sb_log.append("Number of utilized slots in primary demands: " + this.primarySlots + "\n");

		sb_log.append("\nNumber of backup demands: " + this.backupDemands + "\n");
		sb_log.append("Number of routed backup demands: " + this.backupRouted + "\n");
		sb_log.append("Number of utilized slots in backup demands: " + this.backupSlots + "\n");

		sb_log.append("Not routed connections " + (this.initConnections - this.routedConnections) + "\n");
		sb_log.append("Connection Blocking rate: " + connectionBlockingRate + "\n");
	}

	/**
	 * A function to log the properties of a log object into a stringBuilder for a
	 * csv form.
	 * 
	 * @param sb_log The StringBuilder to append the log's information in csv form.
	 */
	public void csvData(StringBuilder sb_csv) {
		sb_csv.append(this.initConnections + ",");
		sb_csv.append(this.routedConnections + ",");
		sb_csv.append(this.time + ",");

		sb_csv.append(this.primaryDemands + ",");
		sb_csv.append(this.primaryRouted + ",");
		sb_csv.append(this.primarySlots + ",");

		sb_csv.append(this.backupDemands + ",");
		sb_csv.append(this.backupRouted + ",");
		sb_csv.append(this.backupSlots + "\n");

	}
}
