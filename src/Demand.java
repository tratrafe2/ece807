import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * This class is responsible for representing the connection demands and
 * therefore hold information about the source, destination and bitrate of the
 * requested connections. What is more, it uses the algorithms from the
 * Graph.java file to calculate the primary and backup paths of the connections
 * depending on the algorithm and backup plan that the user has stated, as well
 * as the number of slots that they need. It is the link between the user
 * options given at the interface and the processes to be performed by the
 * program.
 * 
 * @author cmakri07
 *
 */
public class Demand {
	private static double baudRate = 10.7; // Gbaud for 12.5 Ghz

	int id;
	int src;
	int dest;

	int bitRate;

	ArrayList<Integer> path;
	int slotsNumber;

	private boolean primary;
	private boolean routed;
	private ArrayList<Integer> backupPath;

	/**
	 * Demand constructor
	 * 
	 * @param src     Source node of demand
	 * @param dest    Destination node of demand
	 * @param bitRate BitRate of demand
	 * @param id      ID of demand
	 */
	public Demand(int src, int dest, int bitRate, int id) {
		this.id = id;
		this.src = src;
		this.dest = dest;
		this.bitRate = bitRate;
		this.path = new ArrayList<Integer>();
		this.backupPath = new ArrayList<Integer>();
		this.primary = true;
		this.routed = false;
	}

	/**
	 * It calculates the main and backup path of the demand if they exist by calling
	 * the findPaths function given a graph, an algorithm and a backup method.
	 * Finally, it uses the calculateSlotsNumber function to store the number of
	 * slots its main path needs.
	 * 
	 * @param g The graph
	 * @param a The algorithm
	 * @param b The backup method
	 */
	public void calculatePaths(Graph g, Algorithm a, BackupPlan b) {
		findPaths(a, b, g, src, dest);
		int distance = g.pathDistance(this.path);
		this.slotsNumber = calculateSlotsNumber(bitRate, distance);
	}

	/**
	 * A function that creates the backup demand of its handler if it exists, given
	 * a graph and a new id for the new demand. Its distance is also set again.
	 * 
	 * @param g  The graph
	 * @param id The new ID of the backup demand
	 * @return The generated demand.
	 */
	public Demand makeBackupDemand(Graph g, int id) {
		if (this.backupPath.size() == 0)
			return null;
		Demand d = new Demand(this.src, this.dest, this.bitRate, id);
		d.path = this.backupPath;
		int distance = g.pathDistance(d.path);
		d.slotsNumber = calculateSlotsNumber(bitRate, distance);
		return d;
	}

	/**
	 * A static function to calculate the bits per symbol given the distance of a
	 * path.
	 * 
	 * @param distance
	 * @return
	 */
	private static int findBitsPerSymbol(int distance) {
		if (distance <= 800) // 16QAM
			return 4;
		if (distance <= 1700) // 8QAM
			return 3;
		if (distance <= 4600) // QPSK
			return 2;
		if (distance <= 9300) // BPSK
			return 1;
		return 0;
	}

	/**
	 * A function that given an algorithm, a backup method, a graph and a source and
	 * destination pair, it calculates the available path/paths of that pair. It is
	 * responsible to set both the main and the backup paths of the demand if
	 * possible.
	 * 
	 * @param a    The algorithm
	 * @param b    The backup method
	 * @param g    The graph
	 * @param src  The source node
	 * @param dest The destination node
	 */
	private void findPaths(Algorithm a, BackupPlan b, Graph g, int src, int dest) {
		Graph g1;
		switch (a) {
		case SUURBALLE:
			g1 = g.suurballeGraph(src, dest);
			ArrayList<ArrayList<Integer>> paths = g1.bfsAllPaths(dest, src);
			if (paths.size() > 0)
				this.path = paths.get(0);
			if (paths.size() > 1)
				this.backupPath = paths.get(1);
			break;
		case BELLMAN_FORD:
			this.path = g.bellmanFord(src, dest);
			g1 = backupGraph(b, g, this.path);
			this.backupPath = g1.bellmanFord(src, dest);
			break;
		case DIJKSTRA:
		default:
			this.path = g.dijkstra(src, dest);
			g1 = backupGraph(b, g, this.path);
			this.backupPath = g1.dijkstra(src, dest);
			break;
		}
		// System.out.println(path);
		// System.out.println(backupPath);
	}

	/**
	 * A static function that depending on the Backup method given and the under
	 * examination path, it calls the appropriate graph altering method in order to
	 * return the altered graph.
	 * 
	 * @param b    The backup method
	 * @param g    The graph
	 * @param path The under examination path
	 * @return The altered graph
	 */
	private static Graph backupGraph(BackupPlan b, Graph g, ArrayList<Integer> path) {
		Graph g1;
		switch (b) {
		case NODE_DISJOINT:
			g1 = g.copyNodeLinkDisjointed(path);
			break;
		case LINK_DISJOINT:
			g1 = g.copyLinkDisjointed(path);
			break;
		case NO_BACKUP:
		default:
			g1 = new Graph(0);
			break;
		}
//		g.printGraph();
//		g1.printGraph();
		return g1;
	}

	/**
	 * A function that reads the demands as given in a file. It only reads Integer
	 * values and discards any other characters.
	 * 
	 * @param filePath
	 * @return
	 */
	public static List<Demand> demandsFromFile(String filePath) {
		List<Demand> list = new ArrayList<Demand>();
		try {
			Scanner sc = new Scanner(new File(filePath));
			sc.nextInt(); // To read the number of demands
			int i = 1;
			while (sc.hasNextLine()) {
				int x = sc.nextInt() - 1;
				int y = sc.nextInt() - 1;
				int bitRate = sc.nextInt();
				Demand d = new Demand(x, y, bitRate, i++);
				list.add(d);
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("No such demand file.");
			System.exit(2);
		}
		return list;
	}

	/**
	 * Getter to determine if demand is primary or not.
	 * 
	 * @return if demand is primary or not.
	 */
	public boolean getPrimary() {
		return this.primary;
	}

	/**
	 * Setter to determine if demand is primary or not.
	 * 
	 * @param primary The state of the demand.
	 */
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	/**
	 * A getter to determine if the demand is routed.
	 * 
	 * @return if the demand is routed.
	 */
	public boolean getRouted() {
		return this.routed;
	}

	/**
	 * Setter to determine if demand is routed or not.
	 * 
	 * @param routed if the demand is routed.
	 */
	public void setRouted(boolean routed) {
		this.routed = routed;
	}

	/**
	 * A static function to calculate the number of slots that a demand needs based
	 * on the bitrate, the distance and the global baudRate.
	 * 
	 * @param bitRate
	 * @param distance
	 * @return
	 */
	private static int calculateSlotsNumber(int bitRate, int distance) {
		return (int) Math.ceil((1.0 * bitRate) / (baudRate * findBitsPerSymbol(distance)));
	}

	/**
	 * The main heuristic function that depending on the FFHeuristic, it calls the
	 * appropriate function in order to sort the list of demands as needed.
	 * 
	 * @param demandsList The list of demands
	 * @param ffHeuristic The heuristic
	 */
	public static void sortDemandsList(List<Demand> demandsList, FFHeuristic ffHeuristic) {
		switch (ffHeuristic) {
		case DESC_PATH_SIZE:
			descPathSize(demandsList);
			break;
		case DESC_NUM_SLOTS:
			descSlotNumber(demandsList);
			break;
		case NO_HEURISTIC:
		default:
		}
	}

	/**
	 * A helping heuristic function that sorts a list of demands descending
	 * depending on their needed slot number.
	 * 
	 * @param list The list of demands
	 */
	private static void descSlotNumber(List<Demand> list) {
		Collections.sort(list, new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				int i = -Integer.compare(o1.slotsNumber, o2.slotsNumber);
				if (i == 0)
					return -Integer.compare(o1.path.size(), o2.path.size());
				return i;
			}
		});
	}

	/**
	 * A helping heuristic function that sorts a list of demands descending
	 * depending on their path size.
	 * 
	 * @param list The list of demands
	 */
	private static void descPathSize(List<Demand> list) {
		Collections.sort(list, new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				int i = -Integer.compare(o1.path.size(), o2.path.size());
				if (i == 0)
					return -Integer.compare(o1.slotsNumber, o2.slotsNumber);
				return i;
			}
		});
	}

	@Override
	public String toString() {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		if (this.path == null || this.path.size() == 0)
			return "Undefined path";
		for (int i = 0; i < this.path.size(); i++)
			temp.add(this.path.get(i) + 1);
		return this.id + " " + (this.primary == true ? "[P]" : "[B]") + (this.routed == true ? "[R]" : "[_]")
				+ "\t" + this.slotsNumber + "\t" + this.bitRate + "\t\t" + temp.get(0) + " -> "
				+ temp.get(temp.size() - 1) + "\t" + temp;
	}

	/**
	 * Enum class to distinguish the different Routing Algorithms.
	 * 
	 * @author cmakri07
	 *
	 */
	public enum Algorithm {
		DIJKSTRA, BELLMAN_FORD, SUURBALLE;

		public static Algorithm getAlgorithm(String choice) {
			int c = Integer.parseInt(choice);
			switch (c) {
			case 1:
				return DIJKSTRA;
			case 2:
				return BELLMAN_FORD;
			case 3:
				return SUURBALLE;
			default:
				System.out.println("Wrong type of algorithm.");
				System.exit(3);
			}
			return null;
		}
	}

	/**
	 * Enum class to distinguish the different Backup Plans.
	 * 
	 * @author cmakri07
	 *
	 */
	public enum BackupPlan {
		LINK_DISJOINT, NODE_DISJOINT, NO_BACKUP;

		public static BackupPlan getBackupPlan(String choice) {
			int c = Integer.parseInt(choice);
			switch (c) {
			case 1:
				return LINK_DISJOINT;
			case 2:
				return NODE_DISJOINT;
			case 3:
				return NO_BACKUP;
			default:
				System.out.println("Wrong type of Backup Plan.");
				System.exit(4);
			}
			return null;
		}
	}

	/**
	 * Enum class to distinguish the different heuristics for the First Fit
	 * algorithm.
	 * 
	 * @author cmakri07
	 *
	 */
	public enum FFHeuristic {
		DESC_PATH_SIZE, DESC_NUM_SLOTS, NO_HEURISTIC;

		public static FFHeuristic getFFHeuristic(String choice) {
			int c = Integer.parseInt(choice);
			switch (c) {
			case 1:
				return DESC_PATH_SIZE;
			case 2:
				return DESC_NUM_SLOTS;
			case 3:
				return NO_HEURISTIC;
			default:
				System.out.println("Wrong type of First Fit Heuristic.");
				System.exit(4);
			}
			return null;
		}
	}
}
