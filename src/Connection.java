import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class holds information about the connections that consist of either
 * only a primary demand or both a primary and a backup demand. This class
 * purpose is to make it easy to keep track of which backup demand relates to
 * which primary demand, in order to get accurate results for the metrics which
 * will be exported.
 * 
 * @author cmakri07
 *
 */
public class Connection {
	Demand primary = null;
	Demand backup = null;

	/**
	 * Connection constructor.
	 * 
	 * @param primary The primary demand of a connection.
	 * @param backup  The backup demand of a connection.
	 */
	Connection(Demand primary, Demand backup) {
		this.primary = primary;
		this.backup = backup;
	}

	/**
	 * Add the demands of the connection into a list.
	 * 
	 * @return The list of demands of the connection
	 */
	public List<Demand> getDemands() {
		List<Demand> list = new ArrayList<Demand>();
		if (primary != null)
			list.add(primary);
		if (backup != null)
			list.add(backup);
		return list;
	}

	/**
	 * A static function that sorts all the demands of a list of connections by
	 * their ID.
	 * 
	 * @param connectionList The list of connections
	 * @return The sorted list of demands of all the connections in the list.
	 */
	public static List<Demand> sortAllDemandsID(List<Connection> connectionList) {
		List<Demand> demands = new ArrayList<Demand>();
		for (Connection con : connectionList)
			demands.addAll(con.getDemands());

		Collections.sort(demands, new Comparator<Demand>() {
			@Override
			public int compare(Demand o1, Demand o2) {
				return Integer.compare(o1.id, o2.id);
			}
		});
		return demands;
	}

	/**
	 * 
	 * Checks if the connection is routed by either its primary, its backup or both
	 * its demands.
	 * 
	 * @return
	 */
	public boolean isRouted() {
		if (primary == null && backup == null)
			return false;
		if (backup == null)
			return primary.getRouted();
		if (primary == null)
			return backup.getRouted();
		return primary.getRouted() || backup.getRouted();
	}
}
