import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the links between nodes in each graph. This class is
 * useful for being able to decompose a graph and focus only on the links of the
 * paths that are under examination and therefore match those links with the
 * appropriate connection without violating the given constraints as described
 * in Routing Spectrum Allocation (RSA) Problem. It also holds the solver which
 * runs the two sub problems as described in Project Outline, which result into
 * the final solution of the problem.
 * 
 * @author cmakri07
 *
 */
public class Link {
	final static int MAX_SLOTS = 320;

	int src;
	int dest;
	int[] slots;

	/**
	 * Constructor of a link.
	 * 
	 * @param src  The source node id
	 * @param dest The destination node id
	 */
	public Link(int src, int dest) {
		this.src = src;
		this.dest = dest;
	}

	/**
	 * A helping function that determines if a link can accept a quantity of slots
	 * that begin at position pos without violating the constraints of continuity or
	 * contiguity,
	 * 
	 * @param pos      The initial position of slot fitting
	 * @param quantity The number of slots to fit.
	 * @return If the link is able to accept the quantity of slots that begin at
	 *         position pos without violating any constraints of continuity.
	 */
	private boolean validIndex(int pos, int quantity) {
		if (this.slots == null)
			return true;
		if (pos + quantity < this.slots.length)
			for (int i = pos; i < pos + quantity; i++)
				if (this.slots[i] != 0)
					return false;
		return true;
	}

	/**
	 * A helping static function to check that all links in a link can accept a
	 * quantity of slots that begin at position pos without violating any
	 * constraints of continuity or contiguity.
	 * 
	 * @param links    The list of links to be checked
	 * @param pos      The initial position of slot fitting
	 * @param quantity The number of slots to fit.
	 * @return If ALL the links are able to accept the quantity of slots that begin
	 *         at position pos without violating any constraints of continuity or
	 *         contiguity.
	 */
	private static boolean checkAllValid(ArrayList<Link> links, int pos, int quantity) {
		for (Link l : links)
			if (l.validIndex(pos, quantity) != true)
				return false;
		return true;
	}

	/**
	 * A helping static function to fill all the links in a list by a quantity o
	 * slots with the given value. It is responsible to call the checkAllValid
	 * function until a valid positionis found in order to proceed to the filling,
	 * or until the limit of MAX_SLOTS is reached.
	 * 
	 * @param links    The list of links to fill.
	 * @param value    The value to fill the slots.
	 * @param quantity The number of slots to fill.
	 * @return If the filling of the links was successful.
	 */
	private static boolean fillLinks(ArrayList<Link> links, int value, int quantity) {
		// Find first available position.
		int pos = 0;
		while (checkAllValid(links, pos, quantity) != true)
			pos++;

		if (pos + quantity >= MAX_SLOTS)
			return false;

		// Initialize slots with zeros
		for (Link l : links) {
			if (l.slots == null) {
				l.slots = new int[MAX_SLOTS];
				for (int i = 0; i < MAX_SLOTS; i++)
					l.slots[i] = 0;
			}
		}

		// Set new slots.
		for (Link l : links)
			for (int x = pos; x < pos + quantity; x++)
				l.slots[x] = value;

		return true;
	}

	/**
	 * A static function that given a graph g can create a 2-D array that represents
	 * its links.
	 * 
	 * @param g The graph.
	 * @return The 2-D array of the links of the graph.
	 */
	public static Link[][] createLinks(Graph g) {
		int[][] edges = g.getEdges();
		int n = edges.length;
		Link[][] links = new Link[n][n];
		for (int i = 0; i < n; i++)
			for (int j = i + 1; j < n; j++)
				if (edges[i][j] != 0) {
					links[i][j] = new Link(i, j);
					links[j][i] = new Link(j, i);
				}
		return links;
	}

	/**
	 * A static function that given the links of a graph, and a path, it can return
	 * a list of the links that constitute the path.
	 * 
	 * @param links The links of a graph.
	 * @param path  The path to get the links from.
	 * @return A list of the links that constitute the path.
	 */
	public static ArrayList<Link> getLinksFromPath(Link[][] links, ArrayList<Integer> path) {
		ArrayList<Link> list = new ArrayList<Link>();
		for (int i = 0; i < path.size() - 1; i++) {
			int u = path.get(i);
			int v = path.get(i + 1);
			list.add(links[u][v]);
		}
		return list;
	}

	/**
	 * This is the RSA solver function. Given a graph, an empty list of connections,
	 * a filled list of demands, an algorithm, a backup plan, a first fit heuristic
	 * and a number of initial demands, it first solves the routing problem of all
	 * its given demands, and finally it performs the spectrum allocation solution.
	 * 
	 * @param g              The graph.
	 * @param connectionList The empty connection list to fill.
	 * @param demandsList    The list of demands to route.
	 * @param algorithm      The Routing Algorithm.
	 * @param backupPlan     The Backup plan.
	 * @param ffHeuristic    The First Fit heuristic.
	 * @param initDemands    The initial amount of demands for this instance.
	 * @return The list of links that hold the spectrum allocation
	 */
	public static ArrayList<Link> solveRSA(Graph g, List<Connection> connectionList, List<Demand> demandsList,
			Demand.Algorithm algorithm, Demand.BackupPlan backupPlan, Demand.FFHeuristic ffHeuristic, int initDemands) {

		List<Demand> backupList = new ArrayList<Demand>();
		List<Demand> chosenList = new ArrayList<Demand>();
		for (int i = 0; i < initDemands; i++) {
			Demand primary = demandsList.get(i);
			chosenList.add(primary);
			primary.calculatePaths(g, algorithm, backupPlan);
			Demand backup = primary.makeBackupDemand(g, initDemands + i + 1);
			if (backup != null) {
				backup.setPrimary(false);
				backupList.add(backup);
			}
			connectionList.add(new Connection(primary, backup));
		}

		chosenList.addAll(backupList);
		Link[][] links = Link.createLinks(g);

		Demand.sortDemandsList(chosenList, ffHeuristic);

		for (Demand d : chosenList) {
			ArrayList<Link> demandLinks = Link.getLinksFromPath(links, d.path);
			if (Link.fillLinks(demandLinks, d.id, d.slotsNumber) == true)
				d.setRouted(true);
		}

		ArrayList<Link> usedLinks = new ArrayList<Link>();
		for (int i = 0; i < links.length; i++)
			for (int j = 0; j < links.length; j++)
				if (links[i][j] != null)
					usedLinks.add(links[i][j]);

		return usedLinks;
	}

	@Override
	public String toString() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int x : this.slots)
			list.add(x);
		return (this.src + 1) + "," + (this.dest + 1) + ":\t" + list;
	}
}
