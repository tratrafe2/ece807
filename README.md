# ECE807 Project implementation

## Introduction
The Routing Spectrum Allocation (RSA) problem is a computational problem that given a set of connections in a network, it tries to allocate the frequency slots necessary in order to establish these connections. At the same time it guarantees that there will be no overlaps or discontinuities between those frequencies. Such a problem can be solved either dynamically or statically.
A dynamic approach to the problem can imply complex networks and demands that vary over time. This means that a frequency allocated for a connection at a given time, might not be suitable if chosen to be allocated at a later time.
On the other hand, for a static approach to the problem, a static network and a static set of demands need to be taken into consideration prior to the computation of the Routing Spectrum Allocation. As a result, during a precomputation process, there can be a solution that indeed does not violate any of the constraints listed above. There are various methods for finding a solution, such as a formulation of the problem to Integer Linear Programming (ILP), or by decomposing the routing and spectrum allocation sub problems in order to solve them independently, one after the other.
For the purposes of this project, only the static approach will be examined.

## Motivation
The Routing Spectrum Allocation (RSA) problem for static optical connections is a common question in the field of optical networking.  Bandwidth increases rapidly as time goes by, due to modern demands of multimedia through the internet. As a result, networks that serve these needs, require to follow such growth in order to succeed to their purpose, while being flexible, efficient and cost effective.
Network problems combine a vast spectrum of disciplines and are undoubtedly crucial for the development of the modern world. As a result, examining in depth the wonders of such disputes is essential for both the scientific community and the society as a whole.

## Objectives
The objective of this project is to implement the problem of Routing and Spectrum Allocation (RSA) for static optical connections and finally result in conclusions for different network topologies through experimental procedures. The problem will be split into two sub problems. The former concerns the Routing, which defines how the connections should be implemented in order to ensure as much as possible that the optimal route is being used, while having a backup path in case of a failure. The latter is about the Spectrum Allocation, which describes how these connections can be assigned wavelengths in such a way that efficiency and reliability are guaranteed.
 
## Installation and Usage
In order to compile the source files and run the program on command line, change directory to the src folder and execute the javac and java commands:

```bash
cd src
javac *.java
java Main 
```

From here on, follow the on-screen instructions to enter the arguments needed.
The arguments can be also entered through the command line. The user can skip from one to all the arguments, beginning from the rightmost.

```bash
java Main [G] [D] [L] [M] [N] [A] [B] [H]
```
[G]: The Graph's file location.

[D]: The demand's file location.

[L]: Option to Log the answer to a file:
(Select No for large number of iterations or set higher Heap memory)
1. Yes. Save all iterations.
2. No. Save only last iteration.

[M]: The number of initial demands for the first iteration. <br />
&nbsp;&nbsp;&nbsp;&nbsp; 1 <= M <= max_demands <br />

[N]: The number of initial demands for the last iteration. <br />
&nbsp;&nbsp;&nbsp;&nbsp; M <= N <= max_demands  <br />

[A]: The enumeration of the Routing Algorithms:
1. Dijkstra
2. Bellman-Ford
3. Suurballe

[B]: The enumeration of the Backup methods:
1. Link Disjoint
2. Node & Link Disjoint
3. No backup

[H]: The enumerations of first-fit Heuristics:
1. Descending Path size
2. Descending Number of slots
3. No heuristic (FIFO primary demands, then FIFO backup demands)

The program will iterate N-M+1 times. Each iteration will read M+i demands, where i is the number of the iteration. <br />
For example:
 
```bash
java Main graph.txt demands.txt 5 100 2 1 1 1 
```
This command will run the solver 96 times for the files: graph.txt and demands.txt. The first time will be reading the first 5 demands of the file, while each subsequent iteration will have one more demand, leading to reading the first 100 demands of the file at the 96th iteration. The results will not be logged into a file and the algorithms used will be Dijkstra, with Link disjoint backup method and a heuristic depending on the path cost.
